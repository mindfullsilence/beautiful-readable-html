# Principles of Writing Beautiful, Readable HTML

## All code in any code-base should look like a single person typed it, no matter how many people contributed.
https://github.com/rwaldron/idiomatic.js/

### The following outlines the style of html coding I use when authoring original web content.

### This should be considered a guideline for writing beautiful, readable HTML. Is should not supersede any styles already in place for a project.

See the preface for idiomatic js

## Doctype
A doctype should always be provided. Only the HTML5 doctype should be used. There should be no padding above or
below the doctype. The word "DOCTYPE" should appear in uppercase, followed by a single space, followed by the
string "html" in lowercase, followed immediately by ">", followed immediately by a line ending.
```html
Aim for:
<!DOCTYPE html>
```

## Required Tags & Attributes
The following should always be provided in an HTML document:
### `<html>`

The html should follow immediately after the doctype declaration with no preceding blank lines. The only exception for
this is when providing documentation in the form of a code comment. If providing a code comment between the doctype and
the html tag, no blank lines should precede or follow the comment.

The html tag should always, without fail, provide a lang attribute

```html
Aim for:
<!DOCTYPE html>
<html lang="en-us">
</html>
```
### `<head>`
Head must be present, and must immediately follow the opening `<html>` tag, with no blank lines preceding or succeeding
it.
```html
Aim for:
<html lang="en-us">
    <head>
    </head>
</html>
```

### `<title>`
The title attribute should always be present and appear just after the opening head tag, preceded and followed by a
single space, and be written as though rendered as phrasing or palpable content.
```html
Aim for:
<html lang="en-us">

    <title>Home page</title>

</html>
```
### `<meta>`
Meta tags should directly follow the title tag, with a single blank line preceding them.
Required metadata include charset, http-equiv, and viewport.
```
Aim for:
<html lang="en-us">

    <title>Home Page</title>

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</html>
```
### `<body>`
Body must be defined and must directly follow a single blank line that follows the closing `</head>`
```html
Aim for:
<html>
    <head>
    </head>

    <body>
    </body>
</html>
```
### `<h1>`
Every page should include at least one `h1` tag, preferably either within a `header`, or within the main content of
the page.

## Loading Scripts & Styles
Scripts should be loaded at the end of document, just before the closing `body` tag. Avoid loading any scripts in the
`head`.

## Whitespace
* Never mix spaces and tabs
* Tabs should be soft indents of 2 spaces. Two of these soft tabs should be used when indenting for a new tag child. One
should be used when wrapping a long list of attributes.
* No line should end with blank spaces or tabs.
* Blank lines are encouraged for separating visually discernible sections as they are rendered in the browser.
    * Sections should be externally padded with no more and no less than 2 blank lines
    * Sub sections, such as columns, sidebars, or other visually tangentially related, embedded areas should be
    externally padded with no more and no less than 2 blank lines
* Files should end with a single empty lines, and should not begin with any blank lines.
* The rules which dictates whether a tag is separated from its surrounding content via line returns are as follows:
    * All tags that are standardized as residing in the [Phrasing content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Phrasing_content),
    and [Palpable content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Palpable_content)
    should be included in the same lines as their neighboring content, and not break into their own lines.
    * All tags that are standardized as residing in the [Metadata content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Metadata_content),
    and [Form-associated content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Form-associated_content)
    should be separated onto their own line without any blank line padding
    * All tags that are standardized as residing in the [Embedded content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Embedded_content),
    and [Interactive content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Interactive_content)
    should be preceded by a single blank line
    * All tags that are standardized as residing in the [Sectioning content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Sectioning_content),
    and [Heading content category](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Heading_content)
    should be preceded by two blank lines.
* The above rules should be considered guides. If a tag falls under more than one category, how it is used and how much
it contains should dictate your decision for whether it is separated and by how much. For a more complete and finite
breakdown, see [#line_separation]Line Separations.

## Line Length
Code should be read just as easily as a book read in a native language. One truism that has held throughout print is that
line length can help or hinder, based on its distance to the beginning of the next line. Regardless of screen size,
resolution, or number of lines, line length should be held to a standard that is dictated by the ease of reading code.
Line length should not exceed 120 characters, and all reasonable attempts should be made to keep line length under 80.
Soft wraps should not be needed when reading the code, or writing the code. Code should naturally fall under or within
these limits.

Should a string of html begin to exceed these limits, one should inflect as to the circumstances that have led to a line
reaching such a length. While divitis is something to be avoided, if a set of HTML can be wrapped within a div or a span,
allowing attributes to be spread amongst child and parent, than doing so should not be avoided simply for over-consideration
of semantic conciseness.
```html
Aim for:
<div id="embedded-subscribe-form" class="ajax-form">
    <form action="https://example.com/subscribe/post" method="post" name="embedded-subscribe-form" novalidate>
    </form>
</div>

Avoid:
<form action="https://example.com/subscribe/post" method="post" id="embedded-subscribe-form" name="embedded-subscribe-form" class="ajax-form" target="_blank" novalidate>
</form>
```

Should attributes not be appropriate for spreading across parent and child, their effect on their tag should be considered.
If their purpose is presentational, they should be stripped from the HTML and be scripted within stylesheet code. Brevity
of HTML should be of utmost importance in these circumstances.

Attempts should be made to avoid wrapping attributes for a single tag across multiple lines. However, doing so is
acceptable if the above alternatives cannot be implemented. See [Indentation](#indentation).
```html
Aim for:
<form action="https://example.com/subscribe/post" method="post" id="embedded-subscribe-form"
  name="embedded-subscribe-form" class="ajax-form" target="_blank" novalidate>
</form>

Avoid:
<form action="https://example.com/subscribe/post" method="post" id="embedded-subscribe-form" name="embedded-subscribe-form" class="ajax-form" target="_blank" novalidate>
</form>
```

If indentation is restricting a line to an inconsiderably short length, consider using a template to separate some of
the code into smaller, more digestible chunks. Polymer, PHP, Twig, Mustache, and many others would do well to be
integrated into the project workflow. Such constructs should be considered both a requirement, and common practice in
todays web development. E.g., for the ludicrous table embeds necessary for html emails, consider the following with a
transpiler:
```html
Aim for:
<link rel="import" href="embedded-table.html">
<embedded-table depth="5">
  <!-- Content here -->
</embedded-table>
```

## Closing tags
Tags should always contain a slash (`/`). Even if a tag can be closed implicitly, "explicit is always better than implicit".
Self closing tags should be closed with a " />", herein giving attention the preceding space before the closing slash-bracket.
```html
Aim for:
<img src="file.jpg" alt="a jpeg file" />

Avoid:
<img src="file.jpg" alt="a jpeg file"/>
```

Closing tags should avoid any preceding spacing.
```html
Aim for:
<p>
    The best preparation for <em>tomorrow</em> is doing your best today.
</p>

Avoid:
<p>
    The best preparation for <em>tomorrow </em>is doing your best today.
</p>
```

## Naming
Though many would tend towards the adoption of an ID-less html convention, this severely limits the practical ability to
write readable, beautiful HTML. Whilst many arguments against IDs exist, it is nevertheless a construct of the language,
and any construct should be taken advantage of for its intent to solve a problem otherwise verbose.

Whilst one could always substitute IDs for classnames, and combine classnames at will, this behavior is discouraged and
substituted for a leaner approach involving IDs as contexts, and classnames as re-useable stylistic identifiers which
fall under aforementioned contexts.
```html
Aim for:
<header id="site-header">
    <nav class="horizontal navigation">
    </nav>
</header>
<footer id="site-footer">
    <nav class="vertical navigation">
    </nav>
</footer>

Avoid:
<header>
    <nav class="horizontal navigation header-theme">
    </nav>
</header>
<footer>
    <nav class="vertical navigation footer-theme">
    </nav>
</footer>
```

Under these rules, IDs should be considered as existing across many pages, unless they reside on the root tag (html tag
in most cases) or body tag. The root tag should include an ID corresponding to some unique piece of the pages url,
whilst the body tag should include an id corresponding to the filename of the HTML, minus its extension. Note that
some CMS systems such as WordPress violate this rule, yet it is trivial to override their internal mechanisms.
```html
Aim for:
<html id="page-142">
  <head>
  </head>
  <body id="template-landing-page">
  </body>
</html>

Avoid:
<html>
  <head>
  </head>
  <body class="page-142 template-landing-page">
  </body>
</html>
```
Classes should be named without abbreviation, acronym, or abridgment.
```html
Aim for:
<a class="button" href="http://example.com">
</a>

Avoid:
<a class="btn" href="http://example.com">
</a>
```
Although abridgment seems to be commonplace among the web, we are writing beautiful, readable html. If a system
such as bootstrap is being used, where such abridgments are maintained by a third party, there are two options
to contemplate: an aliasing of abridged terms to their fully qualified counterparts, or, to lesson the burden of
a projects preliminary labor, simply placing such classnames at the end of your class list on an element, and
adding a comment to the beginning of the document, just after the body, stating that such a system is used where
any abridged classnames are seen, will suffice. If the latter, considering a minimal use of such a system, and
give preference to the former option. For these reasons, prefer ZURB foundation over bootstrap for fully
qualified classnames if the choice is available.

Preference should be given to writing out numbers where possible, unless generated by a system.
```html
Aim for:
<div class="twelve-small columns">
</div>

Avoid:
<div class="small-12 columns">
</div>
```
Where multiple classnames are necessary to describe the visual properties of the rendered page, those classnames
should be ordered in such a way that their structure resembles a sentence. Adding such terms as "with", or "and"
may be placed as sentence concatenations without any definitions within the stylesheets associated
with the page, if documented.
```html
Aim for:
<div class="dark-themed accordion-item with six-columns is-active">
</div>

Avoid:
<div class="accordion-item dark-themed is-active six-columns">
</div>
```
Where one opts for such a methodology, classnames which are used within stylesheets should contain dashes to
indicate their use within the stylesheets from the placeholder classnames that simply concatenate statements.
However, it is certainly acceptable to remove such concatenations, while keeping the same cohesiveness, and
allowing one to consider single-word classnames:
```html
Aim for:
<div class="six-columns dark accordion-item is-active">
</div>
```
Note that classname "sentences" should be structured in such a way that the state of the element is placed at
the end of the list. This is to uphold the sentence structure when being affected by external mechanisms such as
javascript, where such classnames are nearly always appended.
```html
Aim for:
<div class="accordion-item is-active">
</div>

Avoid:
<div class="active accordion-item">
</div>
```
## Attribute Ordering
To reduce the length by which a developer must read your code before understanding that any given tag is the tag they
wish to effect some change, it is necessary to give the most descriptive information up-front, and cascading
with less specific information.
```html
Aim for:
<form id="subscription-form" name="subscribe" action="process-form.php" class="dark-themed form">
</form>

Avoid:
<form action="process-form.php" class="dark-themed form" name="subscribe" id="subscription-form">
</form>
```
Custom data attributes should be placed at the end of the tag, regardless of their correlating specificity.
```html
Aim for:
<div
  class="dark-themed six-column accordion"
  data-toggle="accordion"
  data-toggle-multiple="true">

</div>

Avoid:
<div
      data-toggle="accordion"
      class="dark-themed six-column accordion"
      data-toggle-multiple="true">
</div>
```
## Boolean Attributes
Boolean attributes should follow all other attributes except data attributes. You should never set a value, empty or
otherwise, on a boolean attribute.
